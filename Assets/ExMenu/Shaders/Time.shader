﻿Shader "ExMenu/Time"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("PanoramaTexture", 2D) = "white" {}
		_TexChars("Characters", 2D) = "white" {}
	}

		SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Geometry+1" }

		Stencil
		{
			Ref 184
			Comp equal
			Pass replace
		}

		Cull Off
		Lighting Off
		ZTest Always

		CGPROGRAM
		#pragma surface surf Unlit
		#include "UnityCG.cginc"

		half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		struct Input { float2 uv_TexChars; };

		sampler2D _TexChars, _MainTex;
		fixed4 _Color;

		UNITY_INSTANCING_BUFFER_START(Props)
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf(Input IN, inout SurfaceOutput o)
		{
			const float2 uv = IN.uv_TexChars;
			const float2 uvchar = { fmod(uv.x * 5, 1), uv.y };

			const float2 x1 = { 1.0 / 4, 0 };
			const float2 y1 = { 0, 1.0 };

			const uint hmWhich = uv.x / (1.0 / 5 * 2);

			const uint3 hour0 = round(tex2D(_MainTex, x1 * 0.5 + y1).rgb), hour1 = round(tex2D(_MainTex, x1 * 1.5 + y1).rgb);
			const uint3 min0 = round(tex2D(_MainTex, x1 * 2.5 + y1).rgb), min1 = round(tex2D(_MainTex, x1 * 3.5 + y1).rgb);

			const float hourf = hour0.r + hour0.g * 2 + hour0.b * 4 + hour1.r * 8 + hour1.g * 16 + hour1.b * 32;
			const float minutef = min0.r + min0.g * 2 + min0.b * 4 + min1.r * 8 + min1.g * 16 + min1.b * 32;

			const uint hour = uint(hourf) % 24;
			const uint minute = uint(minutef) % 60;

			const uint hm = hmWhich == 0 ? hour : minute;
			const uint hmd = uint(uv.x * 5) % 3 == 0 ? hm / 10 : hm % 10;

			float2 uv0 = { (hmd % 8 + uvchar.x) / 8, 1 - (floor((float)hmd / 8.0) + 1 - uvchar.y) / 2 };
			const float2 uvcoron = { uvchar.x / 8 - 5.0 / 8, uvchar.y / 2 };
			uv0 = uint(uv.x * 5) == 2 ? uvcoron : uv0;

			fixed4 c = tex2D(_TexChars, uv0) * _Color;
			clip(c.a - 0.5);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
		FallBack "Diffuse"
}