Shader "Null's Shader/OverwirteScreen" {
	Properties {
		_Color ("Color", Color) = (1, 1, 1, 1)
		_MainTex ("Main Texture", 2D) = "white" {}
		_Size ("Size", Range(0,1.0)) = 1.
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue" = "Overlay+1000" "IgnoreProjector" = "True" }
		Pass {
			ZTest Always
			ZWrite Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			fixed4 _Color;
			fixed _Size;
			
			struct appdata {
				float4 vertex : POSITION;
				float4 uv : TEXCOORD0;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert (appdata v)
			{
				v2f o;
				float ratio = (_MainTex_TexelSize.x * _ScreenParams.x) / (_MainTex_TexelSize.y * _ScreenParams.y);
				float2 size = _Size * lerp(float2(1, ratio), float2(1 / ratio, 1), ratio < 1);
				o.pos = float4(2 * v.uv.x * size.x - 1, 1 -  2 * v.uv.y * size.y, 1, 1);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return fixed4(_Color * tex2D(_MainTex, i.uv));
			}
			ENDCG			
		}
	} 
}
