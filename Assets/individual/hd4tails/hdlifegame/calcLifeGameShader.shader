﻿Shader "CustomRenderTexture/LifeGame"
{
	Properties
	{
		_InputTex ("InputTex", 2D) = "white" {}
	}
	SubShader
	{
		Lighting Off
		Blend One Zero
		Pass
		{
			Name "Update"
			CGPROGRAM

				#include "UnityCustomRenderTexture.cginc"
				#pragma vertex CustomRenderTextureVertexShader
				#pragma fragment frag

				sampler2D  _InputTex;

				float2 seamlessshift(float2 px,float x,float y)
				{
					float2 uvPerpxx = float2(1.0 / _CustomRenderTextureWidth, 1.0 / _CustomRenderTextureHeight);
					return px + float2(x,y)*uvPerpxx;
				}
				float comborationCalc(float3x3 input, float3x3 combo )
				{
					return 	input._m00 * combo._m00 + input._m01 * combo._m01 + input._m02 * combo._m02 + 
							input._m10 * combo._m10 + input._m11 * combo._m11 + input._m12 * combo._m12 + 
							input._m20 * combo._m20 + input._m21 * combo._m21 + input._m22 * combo._m22;
				}
				half4 frag(v2f_customrendertexture IN) : SV_Target
				{
					float4 output = 1;
					float2 uv = IN.globalTexcoord;
					float4 inputcolor = tex2D(_InputTex,float2(1 - uv.x,uv.y));
					if(inputcolor.a != 0)
					{
						output = lerp(0.2,1.0,0.5 + _SinTime / 2);
						output = inputcolor;
						output.a = 1;
						
					}
					else
					{
						float4 m00 = tex2D(_SelfTexture2D, seamlessshift(uv,-1,-1));
						float4 m01 = tex2D(_SelfTexture2D, seamlessshift(uv,-1,0));
						float4 m02 = tex2D(_SelfTexture2D, seamlessshift(uv,-1,1));
						float4 m10 = tex2D(_SelfTexture2D, seamlessshift(uv,0,-1));
						float4 m11 = tex2D(_SelfTexture2D, seamlessshift(uv,0,0));
						float4 m12 = tex2D(_SelfTexture2D, seamlessshift(uv,0,1));
						float4 m20 = tex2D(_SelfTexture2D, seamlessshift(uv,1,-1));
						float4 m21 = tex2D(_SelfTexture2D, seamlessshift(uv,1,0));
						float4 m22 = tex2D(_SelfTexture2D, seamlessshift(uv,1,1));
						float3x3 before = float3x3(
							m00.a,m01.a,m02.a,
							m10.a,m11.a,m12.a,
							m20.a,m21.a,m22.a
						);
						float3x3 sumwithoutcenter = float3x3(
							1,1,1,1,0,1,1,1,1
						);
						
						float sumwithoutcentervalue = comborationCalc(before,sumwithoutcenter);
						float centervalue = before._m11;

						output = tex2D(_SelfTexture2D, uv);
						if(centervalue == 1)
						{
							if(sumwithoutcentervalue == 3)
							{
								output = (	before._m00 * m00 + before._m01 * m01 + before._m02 * m02 +
											before._m10 * m10 + 				     before._m12 * m12 +
											before._m20 * m20 + before._m21 * m21 + before._m22 * m22
										) / sumwithoutcentervalue;
								output.a = 1;
							}
							if(sumwithoutcentervalue <= 1)
							{
								//output = 1;
								output.a = 0;
							}
							if(sumwithoutcentervalue >= 4)
							{
								//output = 1;
								output.a = 0;
							}
						}
						else
						{
							if(sumwithoutcentervalue == 3)
							{
								output = (	before._m00 * m00 + before._m01 * m01 + before._m02 * m02 +
											before._m10 * m10 + 				     before._m12 * m12 +
											before._m20 * m20 + before._m21 * m21 + before._m22 * m22
										) / sumwithoutcentervalue;
								output.a = 1;
							}
						}
					}
					return  output;

				}
			ENDCG
		}
	}
}