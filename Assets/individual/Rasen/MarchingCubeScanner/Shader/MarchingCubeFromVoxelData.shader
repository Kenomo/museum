﻿Shader "MarchingCubeFromVoxelData"
{
    Properties
    {
        _CustomRenderTexture ("CustomRenderTexture", 3D) = "" {}
		_TopTex ("TopTex", 2D) = "white" {}
        _TopDepth ("TopDepth", 2D) = "white" {}
        _BottomTex ("BottomTex", 2D) = "white" {}
        _BottomDepth ("BottomDepth", 2D) = "white" {}
        _FrontTex ("FrontTex", 2D) = "white" {}
        _FrontDepth ("FrontDepth", 2D) = "white" {}
        _BackTex ("BackTex", 2D) = "white" {}
		_BackDepth ("BackDepth", 2D) = "white" {}
        _RightTex ("RightTex", 2D) = "white" {}
		_RightDepth ("RightDepth", 2D) = "white" {}
        _LeftTex ("LeftTex", 2D) = "white" {}
        _LeftDepth ("LeftDepth", 2D) = "white" {}
        
		_MarchingCubeTex("Marching Cube Tex", 2D) = "white" {}
        _SegmentNum("SegmentNum", int) = 32
		_Threashold("Threshold", Range(0, 1)) = 0.5
        _DiffuseColor("Diffuse",Color) = (0,0,0,1)
		_EmissionIntensity("Emission Intensity", Range(0,1)) = 1
		_EmissionColor("Emission", Color) = (0,0,0,1)

		_StoneTex("Stone (RGB)", 2D)								= "white" {}
		[NoScaleOffset] _StoneBumpTex("Stone Normal Map", 2D)		= "bump" {}
		_PetrifyMask("Petrify Mask (B)", 2D)						= "white" {}
		_PetrifyProgress("Petrify Progress", Range(0,1))			= 0		// 石化進行度
		_PetrifyRate("Petrify Rate", Range(0, 1))					= 0.8	// 石化テクスチャの反映度
		_BoundaryBlurAmount("Boundary Blur Amount", Range(0,50))	= 1		// 境界面のぼかし度

		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
        [MaterialToggle] _IsDisplaySurface("IsDisplaySurface", int) = 0
    }

	SubShader
	{
		// カリングをオフ
		Cull Off
		Tags { "RenderType"="Opaque" }
		
		CGINCLUDE
		#define UNITY_PASS_DEFERRED
		#include "HLSLSupport.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#include "UnityPBSLighting.cginc"
		#include "AutoLight.cginc"
		#include "UnityLightingCommon.cginc"
		#include "Libs/Primitives.cginc"
		#include "Libs/Utils.cginc"


		struct appdata
		{
			float4 vertex	: POSITION;
		};

		struct v2g
		{
			float4 pos : SV_POSITION;
		};

		struct g2f_light
		{
			float4 pos			: SV_POSITION;	// ローカル座標
			float3 normal		: NORMAL;		// 法線
			float4 worldPos		: TEXCOORD0;	// ワールド座標
			fixed4 diff         : COLOR0;
			float4 color        : TEXCOORD1;	// 頂点カラー
		};

        sampler3D _CustomRenderTexture;
		sampler2D _TopTex;
		sampler2D _TopDepth;
        sampler2D _BottomTex;
		sampler2D _BottomDepth;
        sampler2D _FrontTex;
		sampler2D _FrontDepth;
        sampler2D _BackTex;
		sampler2D _BackDepth;
        sampler2D _RightTex;
		sampler2D _RightDepth;
        sampler2D _LeftTex;
		sampler2D _LeftDepth;
		sampler2D _StoneTex;
		sampler2D _StoneBumpTex;
		sampler2D _PetrifyMask;

		int _SegmentNum;
		float _Threashold;

		float4 _DiffuseColor;
		float3 _HalfSize;
		float4x4 _Matrix;

		float _EmissionIntensity;
		half3 _EmissionColor;

		half _Glossiness;
		half _Metallic;
        int _IsDisplaySurface;
		float  _PetrifyProgress;
		float  _PetrifyRate;
		float  _BoundaryBlurAmount;		

		sampler2D _MarchingCubeTex;

		static const float3 vertexOffset[8] = {
			float3(0.0f, 0.0f, 0.0f),float3(1.0f, 0.0f, 0.0f),float3(1.0f, 1.0f, 0.0f),float3(0.0f, 1.0f, 0.0f),
			float3(0.0f, 0.0f, 1.0f),float3(1.0f, 0.0f, 1.0f),float3(1.0f, 1.0f, 1.0f),float3(0.0f, 1.0f, 1.0f)
		};

		static const int2 edgeConnection[12] = {
			int2( 0,1 ),int2( 1,2 ),int2( 2,3 ),int2( 3,0 ),
			int2( 4,5 ),int2( 5,6 ),int2( 6,7 ),int2( 7,4 ),
			int2( 0,4 ),int2( 1,5 ),int2( 2,6 ),int2( 3,7 )
		};

		static const float3 edgeDirection[12] = {
			float3(1.0f, 0.0f, 0.0f),float3(0.0f, 1.0f, 0.0f),float3(-1.0f, 0.0f, 0.0f),float3(0.0f, -1.0f, 0.0f),
			float3(1.0f, 0.0f, 0.0f),float3(0.0f, 1.0f, 0.0f),float3(-1.0f, 0.0f, 0.0f),float3(0.0f, -1.0f, 0.0f),
			float3(0.0f, 0.0f, 1.0f),float3(0.0f, 0.0f, 1.0f),float3(0.0f, 0.0f, 1.0f),float3(0.0f,  0.0f, 1.0f)
		};

        /**
         * テクスチャに保存されているデータを取得する
         */
        float4 getCustomRenderTexture(float3 pos) { 
            float4 ret = tex3Dlod(_CustomRenderTexture, float4(pos.x, pos.y, pos.z, 0));
            return ret;
        }

		float getOffset(float val1, float val2, float desired)
		{
			float delta = val2 - val1;
			if (delta == 0.0) {
				return 0.5;
			}
			return (desired - val1) / delta;
		}

		inline float4 linearToSrgb(float4 c)
		{
			return lerp(c * 12.92, 1.055 * pow(c, 1.0 / 2.4) - 0.055, step(0.0031308, c));
		}

		int getCubeEdgeFlags(int i)
		{
			float x = mod(i, 64.0) / 63.0;
			float y = (int)(i / 64) / 63.0;
#if UNITY_UV_STARTS_AT_TOP
			y = 1.0 - y;
#endif
			//float4 data = linearToSrgb(tex2Dlod(_MainTex, float4(x, y, 0, 0)));
			float4 data = tex2Dlod(_MarchingCubeTex, float4(x, y, 0, 0));

			int r = (int)(data.r * 255);
			int g = (int)(data.g * 255);

			int flag = (r << 8) | g;
			return flag;
		}

		int getTriangleConnectionTable(int i)
		{
			float x = mod(i, 64.0) / 63.0;
			float y = (int)(i / 64) / 63.0;
#if UNITY_UV_STARTS_AT_TOP
			y = 1.0 - y;
#endif
			float4 data = tex2Dlod(_MarchingCubeTex, float4(x, y, 0, 0));

			int b = (int)(data.b * 256);
			return b;
		}

		// 頂点シェーダ
		v2g vert(appdata v)
		{
			v2g o = (v2g)0;
			o.pos = v.vertex;
			return o;
		}

		/** val の小数点第 decimalPlace 位を四捨五入する */
		float round(float val, int decimalPlace) {
			int digits = 1;
			for(int i = 0; i < decimalPlace; i++)
			{
				digits *= 10;
			}
			int temp = val * digits + 5;
			return temp / digits;
		}

		float3 vec3Cross(float3 v1, float3 v2){
			float3 n;
			n.x = v1.y * v2.z - v1.z * v2.y;
			n.y = v1.z * v2.x - v1.x * v2.z;
			n.z = v1.x * v2.y - v1.y * v2.x;
			return n;
		}

		/** 3頂点から面法線を計算する */
		float3 calcSurfaceNormal(float3 pos1, float3 pos2, float3 pos3)
		{
			float3 vv1, vv2, n;
			vv1 = pos2 - pos1;
			vv2 = pos3 - pos2;
			n = vec3Cross(vv1, vv2);
			n = normalize(n);
			return n;
		}

		// 実体のジオメトリシェーダ
		[maxvertexcount(15)]	// シェーダから出力する頂点の最大数の定義
		void geom_light(point v2g input[1], inout TriangleStream<g2f_light> outStream)
		{
			g2f_light o = (g2f_light)0;

			int i, j;
			float  cubeValue[8];	// グリッドの８つの角のスカラー値取得用の配列
			float3 cubeColor[8];	// グリッドの８つの角の頂点カラー配列
			float inside[8];

			// 頂点配列
			float3 edgeVertices[12] = {
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0)
            };

			// 頂点カラー
			float3 edgeColor[12] = {
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0),
				float3(0, 0, 0)
			};

			float3 pos = input[0].pos;

			// グリッドの８つの角に対する値を取得
			for (i = 0; i < 8; i++) {
				float4 data = getCustomRenderTexture(float3(pos.x + vertexOffset[i].x / _SegmentNum, pos.y + vertexOffset[i].y / _SegmentNum, pos.z + vertexOffset[i].z / _SegmentNum));
				cubeColor[i] = data.rgb;
                cubeValue[i] = data.a;
			}

			pos -= float3(1.0, 1.0, 1.0) * 0.5;

			int flagIndex = 0;

			// グリッドの８つの角の値が閾値以上または全方向からみて内側かチェック
			for (i = 0; i < 8; i++) {
				if (cubeValue[i] != 0) {
					flagIndex |= (1 << i);
				}
			}

			int edgeFlags = getCubeEdgeFlags(flagIndex);

			// 空か完全に満たされている場合は何も描画しない
			if ((edgeFlags == 0) || (edgeFlags == 255)) {
				return;
			}

			float offset = 0.5;
			float3 vertex;
			for (i = 0; i < 12; i++) {
				if ((edgeFlags & (1 << i)) != 0) {
					// 角同士の閾値のオフセットを取得
					offset = getOffset(cubeValue[edgeConnection[i].x], cubeValue[edgeConnection[i].y], /*1 - (1.0 / _SegmentNum)*/ _Threashold);
					// オフセットを元に頂点の座標を補完
					vertex = (vertexOffset[edgeConnection[i].x] + offset * edgeDirection[i]);

					edgeVertices[i].x = pos.x + vertex.x / _SegmentNum;
					edgeVertices[i].y = pos.y + vertex.y / _SegmentNum;
					edgeVertices[i].z = pos.z + vertex.z / _SegmentNum;
					edgeColor[i] = (cubeColor[edgeConnection[i].x] + cubeColor[edgeConnection[i].y]) / 2;
				}
			}

			// 頂点を連結してポリゴンを作成
			int vindex = 0;
			int findex = 0;
			// 最大５つの三角形ができる
			for (i = 0; i < 5; i++) {
				findex = flagIndex * 16 + 3 * i;
				if (getTriangleConnectionTable(findex) < 0)
					break;

				int vertexIndex0 = getTriangleConnectionTable(findex + 0);
				int vertexIndex1 = getTriangleConnectionTable(findex + 1);
				int vertexIndex2 = getTriangleConnectionTable(findex + 2);
				float3 surfaceNormal = calcSurfaceNormal(edgeVertices[vertexIndex0], edgeVertices[vertexIndex1], edgeVertices[vertexIndex2]);

				// 三角形を作る
				for (j = 0; j < 3; j++) {
					vindex = getTriangleConnectionTable(findex + j);

					// Transform行列を掛けてワールド座標に変換
					float4 ppos = mul(_Matrix, float4(edgeVertices[vindex], 1));
					o.worldPos = ppos;
					o.pos = UnityObjectToClipPos(ppos);
					o.color = float4(edgeColor[vindex], 1);

					float3 norm = UnityObjectToWorldNormal(surfaceNormal);
					o.normal = normalize(mul(_Matrix, float4(norm, 0)));

					half nl = max(0, dot(o.normal, _WorldSpaceLightPos0.xyz));
					o.diff = nl * _LightColor0;
					o.diff.rgb += ShadeSH9(half4(o.normal, 1));

					outStream.Append(o);	// ストリップに頂点を追加
				}
				outStream.RestartStrip();	// 一旦区切って次のプリミティブストリップを開始
			}
		}

        bool on_texture(float2 uv) {
            float wid = 0.0005;
            return (-wid <= uv.x && uv.x <= 1.0 + wid && -wid <= uv.y && uv.y <= 1.0 + wid);
        }

        bool equalsFloat(float a, float b)
        {
            float margin = 0.05;
            return a - 0.05 < b && b < a + 0.05;
        }
		
        // テクスチャ番号と座標から色を取得する
        float4 get_color(float3 worldPos, float3 normal, float4 color)
        {
			if(normal.z > 0) {
				float2 uv    = worldPos.xy + float2(0.5, 0.5);
				float depth  = tex2D(_FrontDepth, uv).r;
				float height = worldPos.z + 0.5;
				if(abs(1 - depth - height) < 0.1) {
	                float4 col = tex2D(_FrontTex, uv);
                	if(_IsDisplaySurface == 1) {
                    	col = float4(0, depth, 0, 1);
                	}
					return col;
				}
			}
			if(normal.z < 0) {
				float2 uv    = float2(1 - (worldPos.x + 0.5), worldPos.y + 0.5);
				float depth  = tex2D(_BackDepth, uv).r;
				float height = worldPos.z + 0.5;
				if(abs(depth - height) < 0.1) {
					float4 col = tex2D(_BackTex, uv);
                	if(_IsDisplaySurface == 1)
                	{
                    	col = float4(0, depth, 1, 1);
               		}
					return col;
				}
			}
			if(normal.x < 0) {
				float2 uv = worldPos.zy + float2(0.5, 0.5);
				float depth  = tex2D(_RightDepth, uv).r;
				float height = worldPos.x + 0.5;
				if(abs(depth - height) < 0.1) {
					float4 col = tex2D(_RightTex, uv);
					if(_IsDisplaySurface == 1)
					{
						col = float4(1, depth, 0, 1);
					}
					return col;
				}
			}
			if(normal.x > 0) {
				float2 uv = float2(1 - (worldPos.z + 0.5), worldPos.y + 0.5);
				float depth  = tex2D(_LeftDepth, uv).r;
				float height = worldPos.x + 0.5;
				if(abs(1 - depth - height) < 0.1) {
                	float4 col = tex2D(_LeftTex, uv);
					if(_IsDisplaySurface == 1)
					{
						col = float4(0, 1, 1, 1);
					}
					return col;
				}
			}
			if(normal.y < 0) {
				float2 uv = worldPos.xz + float2(0.5, 0.5);
				float depth  = tex2D(_TopDepth, uv).r;
				float height = worldPos.y + 0.5;
				if(abs(depth - height) < 0.1) {
					float4 col = tex2D(_TopTex, uv);
					if(_IsDisplaySurface == 1)
					{
						col = float4(1, 0, 0, 1);
					}
					return col;
				}
			}
			if(normal.y > 0) {
				float2 uv = float2(1 - (worldPos.x + 0.5), worldPos.z + 0.5);
				float depth  = tex2D(_BottomDepth, uv).r;
				float height = worldPos.y + 0.5;
				if(abs(1 - depth - height) < 0.1) {
					float4 col = tex2D(_BottomTex, uv);
					if(_IsDisplaySurface == 1)
                	{
                    	col = float4(1, 0, 1, 1);
                	}
					return col;
				}
			}
			return color;
			//return float4((1 + normal.x) / 2, (1 + normal.y) / 2, (1 + normal.z) / 2, 1);
        }
		
		// 実体のフラグメントシェーダ
		fixed4 frag(g2f_light IN) :SV_Target
		{
			half3 worldViewDir = normalize(UnityWorldSpaceViewDir(IN.worldPos));
			half3 worldRefl    = reflect(-worldViewDir, IN.normal);

			//half4 skyData      = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldRefl);
			//half3 skyColor     = DecodeHDR(skyData, unity_SpecCube0_HDR);

			fixed4 col = get_color(IN.worldPos, IN.normal, IN.color);
			
			// マスク用テクスチャから濃度を取得（青チャンネルの値のみ使用する
            float2 uv = IN.worldPos.xy + float2(0.5, 0.5);
			fixed a = tex2D(_PetrifyMask, uv).b;
			// _BoundryBlurAmount と _PetrifyProgress と マスクテクスチャの値から石化する範囲を決定する
			float b = 51 - _BoundaryBlurAmount;
			a = a * (b - 1) / b;
			fixed petrify_rate	= b * (_PetrifyProgress - a);
			petrify_rate		= max(min(1, petrify_rate), 0);
			// x,y,z を法線情報を元に x,y へマッピング
			float4 xy_col = tex2D(_StoneTex, IN.worldPos.xy + float2(0.5, 0.5));
			float4 xz_col = tex2D(_StoneTex, IN.worldPos.xz + float2(0.5, 0.5));
			float4 yz_col = tex2D(_StoneTex, IN.worldPos.yz + float2(0.5, 0.5));
			float4 result = xy_col * IN.normal.z * IN.normal.z + xz_col * IN.normal.y * IN.normal.y + yz_col * IN.normal.x * IN.normal.x;
			
			float gray = 0.2126 * col.r + 0.7152 * col.g + 0.0722 * col.b;
			gray = (1 - _PetrifyRate) * gray + _PetrifyRate;
			float gray_col = float4(gray, gray, gray, 1);
			result = gray_col * result;
//			col = col * result;
			col = lerp(col, result, petrify_rate);

			//col = (col * (fixed4(1, 1, 1, 1) - IN.diff));
			return col;
		}

		ENDCG

		// 実体のレンダリング
		Pass{
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma target 2.5
			#pragma vertex vert
			#pragma geometry geom_light
			#pragma fragment frag
			#pragma enable_d3d11_debug_symbols
			ENDCG
		}

		
		// ShadowCasterで影だし
        Pass
        {
            Tags {"LightMode" = "ShadowCaster"}
            CGPROGRAM
			#pragma target 2.5
            #pragma vertex vert
            #pragma geometry geom_light
            #pragma fragment frag
            ENDCG
        }
		
	}
	Fallback "VertexLit"
}
