﻿Shader "ModelVoxelization"
{
	Properties
    {
        _TopTex ("TopTex", 2D) = "white" {}
        _TopDepth ("TopDepth", 2D) = "white" {}
        _BottomTex ("BottomTex", 2D) = "white" {}
        _BottomDepth ("BottomDepth", 2D) = "white" {}
        _FrontTex ("FrontTex", 2D) = "white" {}
        _FrontDepth ("FrontDepth", 2D) = "white" {}
        _BackTex ("BackTex", 2D) = "white" {}
		_BackDepth ("BackDepth", 2D) = "white" {}
        _RightTex ("RightTex", 2D) = "white" {}
		_RightDepth ("RightDepth", 2D) = "white" {}
        _LeftTex ("LeftTex", 2D) = "white" {}
        _LeftDepth ("LeftDepth", 2D) = "white" {}
		_SegmentNum("SegmentNum", int) = 32
    }
    
	SubShader
    {
        Cull Off ZWrite Off ZTest Always
        Pass
        {
			// インスペクタに表示したときにわかりやすいように名前を付けておく
            Name "Update"

            CGPROGRAM
			// UnityCustomRenderTexture.cgincをインクルードする
			#include "UnityCustomRenderTexture.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag

			sampler2D _TopTex;
			sampler2D _TopDepth;
			sampler2D _BottomTex;
			sampler2D _BottomDepth;
			sampler2D _FrontTex;
			sampler2D _FrontDepth;
			sampler2D _BackTex;
			sampler2D _BackDepth;
			sampler2D _RightTex;
			sampler2D _RightDepth;
			sampler2D _LeftTex;
			sampler2D _LeftDepth;
			int _SegmentNum;

			/**
         	 * テクスチャに保存されている depth を取得する
        	 */
			float get_depthtex(sampler2D DepthTexture, float2 uv) { 
            	float depth = tex2D(DepthTexture, uv).r;
            	return depth;
        	}
			
			/**
			 * pos が 上から見て有効範囲内か
			 */
			float4 getDistanceBetweenDepthAndPosAtTop(float3 pos) {
				// 上側から撮影したカメラからの距離
				float2 uv = pos.xz;
				float depth = get_depthtex(_TopDepth, uv);
				float4 col  = tex2D(_TopTex, uv);
				float height = pos.y;
				if(depth == 0 || height - (1.0 / _SegmentNum / 2) > depth)
				{
					return float4(0, 0, 0, 0);
				}
				if(height < depth) {
					return float4(col.r, col.g, col.b, 1);
				}
				return float4(col.r, col.g, col.b, 1 - (height - depth) / (1.0 / _SegmentNum / 2));
			}

			/**
			 * pos が 下から見て有効範囲内か
			 */
			float4 getDistanceBetweenDepthAndPosAtBottom(float3 pos) {
				// 下側から撮影したカメラからの距離
				float2 uv = float2(1 - pos.x, pos.z);
				float depth = get_depthtex(_BottomDepth, uv);
				float4 col  = tex2D(_BottomTex, uv);
				float height = 1 - pos.y;
				if(depth == 0 || height - (1.0 / _SegmentNum / 2) > depth)
				{
					return float4(0, 0, 0, 0);
				}
				if(height < depth) {
					return float4(col.r, col.g, col.b, 1);
				}
				return float4(col.r, col.g, col.b, 1 - (height - depth) / (1.0 / _SegmentNum / 2));
			}

			/**
			 * pos が 前から見て有効範囲内か
			 */
			float4 getDistanceBetweenDepthAndPosAtFront(float3 pos) {
				// 前方から撮影したカメラからの距離
				float2 uv = pos.xy;
				float depth = get_depthtex(_FrontDepth, uv);
				float4 col  = tex2D(_FrontTex, uv);
				float height = 1 - pos.z;
				if(depth == 0 || height - (1.0 / _SegmentNum / 2) > depth)
				{
					return float4(0, 0, 0, 0);
				}
				if(height < depth) {
					return float4(col.r, col.g, col.b, 1);
				}
				return float4(col.r, col.g, col.b, 1 - (height - depth) / (1.0 / _SegmentNum / 2));
			}

			/**
			 * pos が 後から見て有効範囲内か
			 */
			float4 getDistanceBetweenDepthAndPosAtBack(float3 pos) {
				// 後方から撮影したカメラからの距離
				float2 uv = float2(1 - pos.x, pos.y);
				float depth = get_depthtex(_BackDepth, uv);
				float4 col  = tex2D(_BackTex, uv);
				float height = pos.z;
				if(depth == 0 || height - (1.0 / _SegmentNum / 2) > depth)
				{
					return float4(0, 0, 0, 0);
				}
				if(height < depth) {
					return float4(col.r, col.g, col.b, 1);
				}
				return float4(col.r, col.g, col.b, 1 - (height - depth) / (1.0 / _SegmentNum / 2));
			}

			/**
			 * pos が 左から見て有効範囲内か
			 */
			float4 getDistanceBetweenDepthAndPosAtLeft(float3 pos) {
				// 左側から撮影したカメラからの距離
				float2 uv = float2(1 - pos.z, pos.y);
				float depth = get_depthtex(_LeftDepth, uv);
				float4 col  = tex2D(_LeftTex, uv);
				float height = 1 - pos.x;
				if(depth == 0 || height - (1.0 / _SegmentNum / 2) > depth)
				{
					return float4(0, 0, 0, 0);
				}
				if(height < depth) {
					return float4(col.r, col.g, col.b, 1);
				}
				return float4(col.r, col.g, col.b, 1 - (height - depth) / (1.0 / _SegmentNum / 2));
			}

			/**
			 * pos が 右から見て有効範囲内か
			 */
			float4 getDistanceBetweenDepthAndPosAtRight(float3 pos) {
				// 右側から撮影したカメラからの距離
				float2 uv = pos.zy;
				float depth = get_depthtex(_RightDepth, uv);
				float4 col  = tex2D(_RightTex, uv);
				float height = pos.x;
				if(depth == 0 || height - (1.0 / _SegmentNum / 2) > depth)
				{
					return float4(0, 0, 0, 0);
				}
				if(height < depth) {
					return float4(col.r, col.g, col.b, 1);
				}
				return float4(col.r, col.g, col.b, 1 - (height - depth) / (1.0 / _SegmentNum / 2));
			}

            float4 frag(v2f_customrendertexture IN) : SV_Target
            {
				float3 pos = IN.globalTexcoord;
				bool enable = true;
				float4 top    = getDistanceBetweenDepthAndPosAtTop   (pos);
				float4 bottom = getDistanceBetweenDepthAndPosAtBottom(pos);
				float4 front  = getDistanceBetweenDepthAndPosAtFront (pos);
				float4 back   = getDistanceBetweenDepthAndPosAtBack  (pos);
				float4 left   = getDistanceBetweenDepthAndPosAtLeft  (pos);
				float4 right  = getDistanceBetweenDepthAndPosAtRight (pos);

				float ave = (top.a + bottom.a + front.a + back.a + left.a + right.a) / 6;
				float4 color = float4(0, 0, 0, 0);
				if(top.a    > color.a) color = top;
				if(bottom.a > color.a) color = bottom;
				if(front.a  > color.a) color = front;
				if(back.a   > color.a) color = back;
				if(left.a   > color.a) color = left;
				if(right.a  > color.a) color = right;			
				
				enable = enable && top.a    != 0;
				enable = enable && bottom.a != 0;
				enable = enable && front.a  != 0;
				enable = enable && back.a   != 0;
				enable = enable && left.a   != 0;
				enable = enable && right.a  != 0;
				
				return float4(color.r, color.g, color.b, enable ? ave : 0);
            }
            ENDCG
        }
    }
}