ギミック作りに便利な部品

以下のインポートが必要
・VRChat SDK
・StandardAssets
・Toybox

v0.18.4:
GetPlayerBasePosの初回取得が上手くいかないバグを多分修正

v0.18.3:
PropSpawnerのバグを修正して最適化

v0.18.2:
VRDiscriminatorを追加

v0.18.1:
PropSpawnerを調整

v0.18.0:
アップデートチェックができるエディタ拡張を追加

v0.17.0:
新しいPropSpawnerを追加

v0.16.3:
PropSpawnerNonTriggerが動作しない事が判明した為削除

v0.16.2:
GetAvatarScaleの修正

v0.16.1:
TPCSlider_Jumpの調整

v0.16.0:
TPCSlider_Jumpの精度を上げたPrefabを追加

v0.15.2:
PropSpawnerNonTriggerの修正

v0.15.1:
EventSelector2FrameDelayのEventSelectorがNoneになっていたバグを修正

v0.15.0:
PropSpawnerNonTriggerのプレハブを追加

v0.14.1:
GetAvatarScaleの修正
AnimationFloatAddを最新版に更新
GetPlayerBasePosを最適化

v0.14.0:
GetPlayerBasePosのプレハブを追加

v0.13.2:
UI用テクスチャのSpriteBorderを調整

v0.13.1:
UI用テクスチャの調整

v0.13.0:
UI用のテクスチャにButtonEnabledを追加

v0.12.1:
uGUISliderの修正

v0.12.0:
DataStorageのプレハブを追加

v0.11.0:
UI用の汎用テクスチャを追加

v0.10.0:
uGUISliderのプレハブを追加
バージョン表記を「メジャー.マイナー.パッチ」へ変更

v0.9:
GetAvatarScaleのプレハブを追加

v0.81:
getGroundが正常に動かないバグを修正

v0.8:
SetupとUpdateのプレハブを追加

v0.72:
TPCSlider_Forward微修正

v0.71:
TPCSlider_ForwardのSliderをScriptSliderに変更

v0.7:
TPCSlider_Forwardのプレハブを追加

v0.61
AddFloat修正

v0.6:
AddFloatのプレハブを追加

v0.5:
UIShape付のCanvasプレハブを追加
Panoramaのプレハブを追加

v0.4:
InteractPortalのプレハブを追加
他プレハブのInspectorを整理

v0.31:
LICENSE更新（LICENSE.txt）

v0.3:
ResetTransform.animを追加
DebrisSpawnとFireSpawnのプレハブを追加
TimedObjectActivatorを使ったTimerのプレハブを追加